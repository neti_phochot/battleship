﻿using UnityEngine;

namespace BattleShip.Map
{
    public class Region : MonoBehaviour, IVisible
    {
        [SerializeField] private LayerMask regionLayerMask;
        [SerializeField] private Transform spawnPoint;
        [SerializeField] private Transform setupCamPoint;

        private MeshRenderer _area;
        public Transform SpawnPoint => spawnPoint;
        public Transform SetupCamPoint => setupCamPoint;
        public LayerMask RegionLayerMask => regionLayerMask;

        private void Awake()
        {
            _area = GetComponent<MeshRenderer>();
        }

        public void Show()
        {
            _area.enabled = true;
        }

        public void Hide()
        {
            _area.enabled = false;
        }
    }
}