﻿using System;
using BattleShip.Pool;
using BattleShip.System;
using UnityEngine;

namespace BattleShip.Ship
{
    public class Cannon : MonoBehaviour
    {
        private const float BulletSpeed = 15f;

        [SerializeField] private Transform firePoint;
        public int MaxAmmo { get; private set; }
        public int Ammo { get; private set; }
        public float Damage { get; private set; }

        private void Awake()
        {
            Debug.Assert(firePoint != null, "firePoint can't be null");
        }

        public event Action<int> OnAmmoChanged;
        public event Action<Bullet> OnFire;

        public void Init(int ammo, float damage)
        {
            MaxAmmo = ammo;
            Ammo = ammo;
            Damage = damage;
        }

        public void Reload()
        {
            Ammo = MaxAmmo;
            OnAmmoChanged?.Invoke(Ammo);
        }

        public void Fire()
        {
            if (Ammo <= 0) return;

            Ammo--;

            var velocity = firePoint.forward * (QuickTime.Instance.Check() * BulletSpeed);
            QuickTime.Instance.Reset();
            var bullet = BulletPool.Instance.Request();
            bullet.Init(firePoint.position, velocity, Damage);
            OnFire?.Invoke(bullet);
            OnAmmoChanged?.Invoke(Ammo);
        }
    }
}