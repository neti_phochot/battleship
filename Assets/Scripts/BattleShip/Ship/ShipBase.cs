using System;
using BattleShip.Ship.ScriptableObjects;
using UnityEngine;

namespace BattleShip.Ship
{
    [RequireComponent(typeof(CannonController))]
    [RequireComponent(typeof(ShipController))]
    [RequireComponent(typeof(Cannon))]
    public class ShipBase : Entity, IDamageable
    {
        [SerializeField] private ShipConfig shipConfig;

        private CannonController _cannonController;
        private ShipController _shipController;
        public bool IsDead { get; private set; }
        public float Health { get; private set; }
        public float MaxHealth { get; private set; }
        public float Fuel { get; private set; }
        public float MaxFuel => shipConfig.Fuel;
        public bool IsEnoughFuel => Fuel > 0f;
        public Cannon Cannon { get; private set; }

        private void Awake()
        {
            Debug.Assert(shipConfig != null, "shipConfig can't be null!");

            _shipController = GetComponent<ShipController>();
            _cannonController = GetComponent<CannonController>();
            Cannon = GetComponent<Cannon>();

            Cannon.Init(shipConfig.Ammo, shipConfig.Attack);
            Health = shipConfig.Health;
            MaxHealth = shipConfig.Health;
            Fuel = shipConfig.Fuel;
        }

        public void Damage(float damage)
        {
            if (damage <= 0f) return;
            Health -= damage;
            OnHealthChanged?.Invoke(Health);
            if (Health > 0f) return;
            IsDead = true;
            Destroy(gameObject);

            OnShipDestroyedEvent?.Invoke(this);
        }

        public event Action<float> OnFuelChanged;
        public event Action<float> OnHealthChanged;
        public event Action<ShipBase> OnShipDestroyedEvent;

        public void ControlCannon(float horizontal, float vertical)
        {
            _cannonController.SetHorizontal(horizontal);
            _cannonController.SetVertical(vertical);
        }

        public void ControlShip(float horizontal, float vertical)
        {
            if (!IsEnoughFuel) return;

            if (Math.Abs(vertical + horizontal) > 0.001f)
            {
                Fuel -= Time.deltaTime;
                if (Fuel < 0f) Fuel = 0f;
                OnFuelChanged?.Invoke(Fuel);
            }

            _shipController.SetVertical(vertical);
            _shipController.SetHorizontal(horizontal);
        }

        public void Fire()
        {
            Cannon.Fire();
        }

        public void Reload()
        {
            Cannon.Reload();
        }

        public void RefillFuel()
        {
            Fuel = shipConfig.Fuel;
            OnFuelChanged?.Invoke(Fuel);
        }
    }
}