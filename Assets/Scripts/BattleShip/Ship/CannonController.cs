﻿using System;
using UnityEngine;

namespace BattleShip.Ship
{
    public class CannonController : MonoBehaviour, IController
    {
        [SerializeField] private RotateAngle xAngle;
        [SerializeField] private RotateAngle yAngle;
        [SerializeField] private float xRotateSpeed;
        [SerializeField] private float yRotateSpeed;
        [SerializeField] private Transform cannonTransform;

        private float _horizontal;

        private float _rotationX;
        private float _rotationY;
        private float _vertical;

        private void FixedUpdate()
        {
            UpdateRotation();
        }

        public void SetHorizontal(float value)
        {
            _horizontal = value;
        }

        public void SetVertical(float value)
        {
            _vertical = value;
        }

        private void UpdateRotation()
        {
            _rotationX += -_vertical * xRotateSpeed * Time.deltaTime;
            _rotationY += _horizontal * yRotateSpeed * Time.deltaTime;

            _rotationX = Mathf.Clamp(_rotationX > 180f ? _rotationX - 360f : _rotationX, yAngle.min, yAngle.max);
            _rotationY = Mathf.Clamp(_rotationY > 180f ? _rotationY - 360f : _rotationY, xAngle.min, xAngle.max);

            cannonTransform.localEulerAngles = new Vector3(_rotationX, _rotationY, 0f);

            _vertical = 0f;
            _horizontal = 0f;
        }

        [Serializable]
        public struct RotateAngle
        {
            public float min;
            public float max;
        }
    }
}