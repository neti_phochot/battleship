﻿using BattleShip.Manager;

namespace BattleShip.State
{
    public abstract class GameState
    {
        protected readonly GameManager GameManager;
        private State _currentState;
        private GameState _gameState;

        protected GameState(GameManager gameManager)
        {
            GameManager = gameManager;
            _gameState = this;
            _currentState = State.Enter;
        }

        public void SetNextState(GameState gameState)
        {
            _gameState = gameState;
            Exit();
        }

        protected virtual void Enter()
        {
            _currentState = State.Update;
        }

        protected virtual void Update()
        {
            _currentState = State.Update;
        }

        protected virtual void Exit()
        {
            _currentState = State.Exit;
        }

        public GameState Process()
        {
            switch (_currentState)
            {
                case State.Enter:
                    Enter();
                    break;
                case State.Update:
                    Update();
                    break;
                case State.Exit:
                    break;
                default:
                    Exit();
                    break;
            }

            return _gameState;
        }

        private enum State
        {
            Enter,
            Update,
            Exit
        }
    }
}