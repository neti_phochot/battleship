﻿using BattleShip.Manager;
using BattleShip.Ship;
using UnityEngine;

namespace BattleShip.State
{
    public class NextTurnState : GameState
    {
        private ShipBase _activeShip;

        public NextTurnState(GameManager gameManager) : base(gameManager)
        {
        }

        protected override void Enter()
        {
            base.Enter();

            var team = GameManager.NextTurn();
            _activeShip = team.ActivePlayer;
            _activeShip.Reload();
            _activeShip.RefillFuel();
        }

        protected override void Update()
        {
            base.Update();

            ShipController();

            if (_activeShip.Cannon.Ammo == 0 || _activeShip.IsDead)
            {
                var delay = GameManager.Instance.GameConfig.TransitionDelay;
                var transition = new TransitionState(GameManager, new NextTurnState(GameManager), delay);
                SetNextState(transition);
            }
        }

        protected override void Exit()
        {
            base.Exit();
            GameManager.Instance.EndTurn();
        }

        private void ShipController()
        {
            if (Input.GetKeyDown(KeyCode.Space)) _activeShip.Fire();

            _activeShip.ControlCannon(Input.GetAxis("CannonHorizontal"), Input.GetAxis("CannonVertical"));
            _activeShip.ControlShip(Input.GetAxis("Horizontal"), Input.GetAxis("Vertical"));
        }
    }
}