﻿using BattleShip.Manager;
using BattleShip.System;
using UnityEngine;

namespace BattleShip.Listener
{
    public class GameCameraListener : BaseListener
    {
        private Bullet _currentBullet;
        private Transform _currentTarget;

        protected override void Initialize()
        {
            GameManager.Instance.OnNextTurn += OnNextTurnEvent;
            GameManager.Instance.OnShipSetupModeChanged += OnShipSetupModeChangedEvent;
            GameManager.Instance.OnShipFired += OnShipFiredEvent;
        }

        protected override void Uninitialized()
        {
            GameManager.Instance.OnNextTurn -= OnNextTurnEvent;
            GameManager.Instance.OnShipSetupModeChanged -= OnShipSetupModeChangedEvent;
            GameManager.Instance.OnShipFired -= OnShipFiredEvent;
        }


        private void OnShipSetupModeChangedEvent(Team team, ObjectEditorManager.EditMode editMode)
        {
            if (editMode == ObjectEditorManager.EditMode.On)
            {
                GameCamara.Instance.LookAt(team.TeamRegion.SetupCamPoint);
                GameCamara.Instance.Follow(team.TeamRegion.SetupCamPoint);
            }
        }

        private void OnNextTurnEvent(Team team)
        {
            _currentTarget = team.ActivePlayer.transform;
            GameCamara.Instance.LookAt(_currentTarget);
            GameCamara.Instance.Follow(_currentTarget);
        }

        private void OnShipFiredEvent(Bullet bullet)
        {
            _currentBullet = bullet;
            _currentBullet.OnHit += ResetCam;
            //GameCamara.Instance.LookAt(bullet.transform);
        }

        private void ResetCam(Collision other)
        {
            _currentBullet.OnHit -= ResetCam;
            GameCamara.Instance.LookAt(_currentTarget);
            GameCamara.Instance.Follow(_currentTarget);
        }
    }
}