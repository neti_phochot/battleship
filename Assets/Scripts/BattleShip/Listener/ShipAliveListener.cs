﻿using BattleShip.Manager;
using BattleShip.Ship;
using BattleShip.UI.Game;
using UnityEngine;

namespace BattleShip.Listener
{
    public class ShipAliveListener : BaseListener
    {
        [SerializeField] private TeamCountUI teamCountUI;

        private void Awake()
        {
            Debug.Assert(teamCountUI != null, "teamCountUI can't be null!");
        }

        protected override void Initialize()
        {
            Debug.Assert(teamCountUI != null, "teamCountUI can't be null!");

            GameManager.Instance.OnSetup += OnOnSetupEvent;
            GameManager.Instance.OnShipDestroyed += OnShipDestroyedEvent;
        }

        protected override void Uninitialized()
        {
            GameManager.Instance.OnSetup -= OnOnSetupEvent;
            GameManager.Instance.OnShipDestroyed -= OnShipDestroyedEvent;
        }

        private void OnOnSetupEvent()
        {
            teamCountUI.Show();
            teamCountUI.SetTeamCount(
                GameManager.Instance.TeamA,
                GameManager.Instance.TeamB);
        }


        private void OnShipDestroyedEvent(ShipBase ship)
        {
            teamCountUI.SetTeamCount(GameManager.Instance.TeamA, GameManager.Instance.TeamB);
        }
    }
}