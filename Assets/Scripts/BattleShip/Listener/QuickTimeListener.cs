﻿using BattleShip.Manager;
using BattleShip.System;
using UnityEngine;
using UnityEngine.UI;

namespace BattleShip.Listener
{
    public class QuickTimeListener : BaseListener
    {
        [SerializeField] private Slider skillBar;

        private void Awake()
        {
            Debug.Assert(skillBar != null, "skillBar can't be null!");
        }

        protected override void Initialize()
        {
            skillBar.gameObject.SetActive(false);

            GameManager.Instance.OnNextTurn += OnNextTurnEvent;
            GameManager.Instance.OnEndTurn += OnEndTurnEvent;

            QuickTime.Instance.OnValueChanged += OnValueChangedEvent;
            QuickTime.Instance.OnVisibleChanged += OnVisibleChangedEvent;
        }

        protected override void Uninitialized()
        {
            GameManager.Instance.OnNextTurn -= OnNextTurnEvent;
            GameManager.Instance.OnEndTurn -= OnEndTurnEvent;

            QuickTime.Instance.OnValueChanged -= OnValueChangedEvent;
            QuickTime.Instance.OnVisibleChanged -= OnVisibleChangedEvent;
        }

        private void OnNextTurnEvent(Team team)
        {
            QuickTime.Instance.Show();
        }

        private void OnEndTurnEvent(Team team)
        {
            QuickTime.Instance.Hide();
        }

        private void OnValueChangedEvent(float value)
        {
            skillBar.value = value;
        }

        private void OnVisibleChangedEvent(bool visible)
        {
            skillBar.gameObject.SetActive(visible);
        }
    }
}