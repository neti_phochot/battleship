﻿using BattleShip.Manager;
using BattleShip.System;
using UnityEngine;

namespace BattleShip.Listener
{
    public class FadeScreenListener : BaseListener
    {
        [SerializeField] private float fadeInLength;

        protected override void Initialize()
        {
            GameManager.Instance.OnNextTurn += OnNextTurnEvent;
            GameManager.Instance.OnShipSetupModeChanged += OnShipSetupModeChangedEvent;
        }

        protected override void Uninitialized()
        {
            GameManager.Instance.OnNextTurn -= OnNextTurnEvent;
            GameManager.Instance.OnShipSetupModeChanged -= OnShipSetupModeChangedEvent;
        }

        private void OnNextTurnEvent(Team team)
        {
            FadeScreen.FadeIn(fadeInLength);
        }

        private void OnShipSetupModeChangedEvent(Team arg1, ObjectEditorManager.EditMode arg2)
        {
            if (arg2 != ObjectEditorManager.EditMode.On) return;
            FadeScreen.FadeIn(fadeInLength);
        }
    }
}