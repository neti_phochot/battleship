﻿using System;
using Singleton;
using UnityEngine;
using Utils;

namespace BattleShip.System
{
    public class FadeScreen : MonoSingleton<FadeScreen>
    {
        [SerializeField] private CanvasGroup overlayCanvasGroup;

        [Header("INIT")] [SerializeField] private bool onAwake;
        [SerializeField] private float fadeInTime;
        private Action _onComplete;

        public override void Awake()
        {
            base.Awake();
            overlayCanvasGroup.gameObject.SetActive(false);
        }

        private void Start()
        {
            if (onAwake)
                FadeIn(fadeInTime);
        }

        public static FadeScreen FadeIn(float lenght)
        {
            Instance.Fade(lenght, true);
            return Instance;
        }

        public static FadeScreen FadeOut(float lenght)
        {
            Instance.Fade(lenght, false);
            return Instance;
        }

        public void setOnComplete(Action action)
        {
            _onComplete = action;
        }

        private void Fade(float lenght, bool fadeIn)
        {
            overlayCanvasGroup.gameObject.SetActive(true);
            _onComplete = () => { };
            var from = fadeIn ? 1f : 0f;
            var to = fadeIn ? 0f : 1f;

            JUtils.Value(from, to, lenght)
                .setOnUpdate(v => { overlayCanvasGroup.alpha = v; })
                .setOnComplete(() =>
                {
                    if (fadeIn)
                        overlayCanvasGroup.gameObject.SetActive(false);
                    _onComplete();
                });
        }
    }
}