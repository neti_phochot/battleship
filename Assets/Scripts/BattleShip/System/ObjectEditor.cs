﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace BattleShip.System
{
    public class ObjectEditor : MonoBehaviour
    {
        [SerializeField] private LayerMask groundLayerMask;
        [SerializeField] private LayerMask overlapLayerMask;

        private Camera _cam;
        private Vector3 _previousLocation;

        private void Awake()
        {
            _cam = Camera.main;
        }

        private void OnMouseDown()
        {
            if (!enabled) return;
            SaveLocation();
        }

        private void OnMouseDrag()
        {
            if (!enabled) return;
            MoveObject();
        }

        private void OnMouseUp()
        {
            if (!enabled) return;
            CheckOverlapCollider();
        }

        public void SetLayerMask(LayerMask layerMask)
        {
            groundLayerMask = layerMask;
        }

        private void MoveObject()
        {
            var ray = _cam.ScreenPointToRay(Input.mousePosition);
            if (!Physics.Raycast(ray, out var hit, Mathf.Infinity, groundLayerMask)) return;
            transform.position = hit.point;
        }

        private void CheckOverlapCollider()
        {
            if (GetOverlapCollider().Count() > 0)
                ResetLocation();
        }

        private IEnumerable<Collider> GetOverlapCollider()
        {
            return Physics
                .OverlapBox(
                    gameObject.transform.position,
                    GetComponent<Collider>().bounds.extents,
                    Quaternion.identity, overlapLayerMask)
                .Where(c => c != GetComponent<Collider>());
        }

        private void SaveLocation()
        {
            _previousLocation = transform.position;
        }

        private void ResetLocation()
        {
            transform.position = _previousLocation;
        }
    }
}