﻿using BattleShip.Map.ScriptableObjects;
using BattleShip.UI.Main_Menu;
using UnityEngine;
using UnityEngine.UI;

namespace BattleShip.Manager
{
    public class MainMenuManager : MonoBehaviour
    {
        [SerializeField] private MapConfig[] mapConfigs;
        [SerializeField] private Button quitButton;

        [SerializeField] private MapSelectorUI mapSelectorUI;
        [SerializeField] private CreditUI creditUI;

        private void Awake()
        {
            Debug.Assert(mapConfigs != null, "mapConfigs can't be null!");
            Debug.Assert(quitButton != null, "quitButton can't be null!");

            Debug.Assert(mapSelectorUI != null, "mapSelector can't be null!");
            Debug.Assert(creditUI != null, "creditUI can't be null!");

            mapSelectorUI.Init(mapConfigs);
            creditUI.Init();
            quitButton.onClick.AddListener(Quit);
        }

        public void Quit()
        {
            SoundManager.Play(SoundType.Button);
            Application.Quit();
        }
    }
}