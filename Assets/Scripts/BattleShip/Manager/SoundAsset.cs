﻿using Singleton;
using UnityEngine;

namespace BattleShip.Manager
{
    public enum SoundType
    {
        Button,
        Warning
    }

    public class SoundAsset : ResourceSingleton<SoundAsset>
    {
        [SerializeField] private AudioClip button;
        [SerializeField] private AudioClip warning;

        public override void Awake()
        {
            base.Awake();

            Debug.Assert(button != null, "button can't be null!");
            Debug.Assert(warning != null, "warning can't be null!");
        }

        public AudioClip GetSound(SoundType soundType)
        {
            AudioClip audioClip = null;
            switch (soundType)
            {
                case SoundType.Button:
                    audioClip = button;
                    break;
                case SoundType.Warning:
                    audioClip = warning;
                    break;
            }

            return audioClip;
        }
    }
}