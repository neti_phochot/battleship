﻿using System;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace BattleShip.Manager
{
    public class SceneLoader : MonoBehaviour
    {
        [Serializable]
        public enum SceneType
        {
            Intro = 0,
            MainMenu = 1
        }

        [SerializeField] private SceneType sceneType;

        public static void LoadScene(SceneType sceneType)
        {
            SceneManager.LoadScene((int) sceneType);
        }

        public static void LoadScene(int sceneIndex)
        {
            SceneManager.LoadScene(sceneIndex);
        }

        public static void Reload()
        {
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
        }

        public void Load()
        {
            LoadScene(sceneType);
        }
    }
}