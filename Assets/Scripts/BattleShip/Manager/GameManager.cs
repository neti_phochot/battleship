﻿using System;
using BattleShip.Factory;
using BattleShip.Map;
using BattleShip.ScriptableObjects;
using BattleShip.Ship;
using BattleShip.State;
using BattleShip.System;
using Singleton;
using UnityEngine;
using Utils;

namespace BattleShip.Manager
{
    public class GameManager : MonoSingleton<GameManager>
    {
        public enum State
        {
            Waiting,
            Setup,
            Started,
            End
        }

        [Header("GAME")] [SerializeField] private GameConfig gameConfig;
        [Header("TEAM")] [SerializeField] private TeamData teamData;
        [Header("REGION")] [SerializeField] private Region teamARegion;
        [SerializeField] private Region teamBRegion;

        [Header("LAYER MASK")] [SerializeField]
        private LayerMask waterLayerMask;

        [SerializeField] private LayerMask shipLayerMask;
        [SerializeField] private LayerMask obstacleLayerMask;

        private GameState _gameState;
        private State _state;

        public bool IsGameOver => _state == State.End;
        public bool IsTeamATurn { get; private set; }
        public Team TeamA { get; private set; }
        public Team TeamB { get; private set; }
        public Team ActiveTeam => IsTeamATurn ? TeamA : TeamB;
        public GameConfig GameConfig => gameConfig;

        public override void Awake()
        {
            base.Awake();
            Debug.Assert(teamARegion != null, "teamARegion can't be null!");
            Debug.Assert(teamBRegion != null, "teamBRegion can't be null!");
        }

        private void Start()
        {
            PreStarted();
        }

        private void Update()
        {
            _gameState = _gameState?.Process();
        }

        public event Action<Team, ObjectEditorManager.EditMode> OnShipSetupModeChanged;
        public event Action OnPreStarted;
        public event Action OnSetup;
        public event Action OnStarted;
        public event Action<Team> OnNextTurn;
        public event Action<Team> OnEndTurn;
        public event Action<Team> OnGameOver;
        public event Action<Bullet> OnShipFired;
        public event Action<HitType, Vector3> OnBulletHit;
        public event Action<ShipBase> OnShipDamage;
        public event Action<ShipBase> OnShipDestroyed;
        public event Action<int> OnShipAmmoChanged;
        public event Action OnButtonClick;

        private void PreStarted()
        {
            _state = State.Waiting;
            SpawnTeam();
            OnPreStarted?.Invoke();
        }

        private void SpawnTeam()
        {
            TeamA = new Team(teamData.teamAName, BuildTeam(teamARegion.SpawnPoint, 0), teamARegion);
            TeamB = new Team(teamData.teamBName, BuildTeam(teamBRegion.SpawnPoint, 1), teamBRegion);
        }

        public void Setup()
        {
            if (_state != State.Waiting) return;
            _state = State.Setup;
            _gameState = new ShipSetupState(this);
            OnSetup?.Invoke();
        }

        public void StartGame()
        {
            _state = State.Started;
            _gameState.SetNextState(new NextTurnState(this));
            OnStarted?.Invoke();
        }

        public Team GetNextTeam()
        {
            IsTeamATurn = !IsTeamATurn;
            var team = ActiveTeam;
            var ship = team.GetRandomMember();
            team.SetActiveMember(ship);
            return team;
        }

        public Team NextTurn()
        {
            var team = GetNextTeam();
            OnNextTurn?.Invoke(team);
            return team;
        }

        public void EndTurn()
        {
            OnEndTurn?.Invoke(ActiveTeam);
        }

        private Team GetWinnerTeam()
        {
            if (TeamA.Alive == 0)
                return TeamB;
            return TeamB.Alive == 0 ? TeamA : null;
        }

        private void CheckGameOver()
        {
            var winnerTeam = GetWinnerTeam();
            if (winnerTeam == null) return;

            _gameState.SetNextState(new GameOverState(this));
            _state = State.End;
            OnGameOver?.Invoke(winnerTeam);
        }

        public void Restart()
        {
            SceneLoader.Reload();
        }

        public void MainMenu()
        {
            SceneLoader.LoadScene(SceneLoader.SceneType.MainMenu);
        }

        private ShipBase[] BuildTeam(Transform spawnPoint, int styleIndex)
        {
            var team = ShipFactory.Instance.Create(teamData.ships, styleIndex);
            var padding = Vector3.zero;
            foreach (var ship in team)
            {
                var shipTransform = ship.transform;
                shipTransform.position = spawnPoint.position + padding;
                shipTransform.rotation = spawnPoint.rotation;
                padding += new Vector3(2f, 0f, 0f);

                ship.OnShipDestroyedEvent += OnShipDestroyedEvent;
                ship.OnHealthChanged += health => { OnShipDamageEvent(ship); };
                ship.Cannon.OnFire += OnShootEvent;
                ship.Cannon.OnAmmoChanged += OnShipAmmoChangedEvent;
            }

            return team;
        }

        [Serializable]
        public struct TeamData
        {
            public string teamAName;
            public string teamBName;
            public ShipFactory.ShipType[] ships;
        }

        #region GAME EVENT

        public void SetTeamEditMode(Team team, bool active)
        {
            var setupMode = active ? ObjectEditorManager.EditMode.On : ObjectEditorManager.EditMode.Off;
            foreach (var ship in team.Members)
            {
                ship.GetComponent<ObjectEditorManager>()
                    .SetEditMode(setupMode);

                ship.GetComponent<ObjectEditor>()
                    .SetLayerMask(team.TeamRegion.RegionLayerMask);
            }

            OnShipSetupModeChanged?.Invoke(team, setupMode);
        }

        private void OnShipDestroyedEvent(ShipBase ship)
        {
            ship.OnShipDestroyedEvent -= OnShipDestroyedEvent;
            ship.Cannon.OnFire -= OnShootEvent;
            ship.Cannon.OnAmmoChanged -= OnShipAmmoChangedEvent;

            OnShipDestroyed?.Invoke(ship);
            CheckGameOver();
        }

        private void OnShootEvent(Bullet bullet)
        {
            OnShipFired?.Invoke(bullet);
            bullet.OnHit += OnBulletHitEvent;
        }

        private void OnBulletHitEvent(Collision collision)
        {
            var layer = collision.gameObject.layer;
            var hitType = HitType.Water;
            if (BitBoard.CheckLayerMask(waterLayerMask, layer))
                hitType = HitType.Water;
            else if (BitBoard.CheckLayerMask(shipLayerMask, layer))
                hitType = HitType.Ship;
            else if (BitBoard.CheckLayerMask(obstacleLayerMask, layer)) hitType = HitType.Obstacle;

            OnBulletHit?.Invoke(hitType, collision.GetContact(0).point);
        }

        private void OnShipAmmoChangedEvent(int ammo)
        {
            OnShipAmmoChanged?.Invoke(ammo);
        }

        private void OnShipDamageEvent(ShipBase ship)
        {
            OnShipDamage?.Invoke(ship);
        }

        public void OnButtonClickEvent()
        {
            OnButtonClick?.Invoke();
        }

        #endregion
    }
}