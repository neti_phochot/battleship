﻿using BattleShip.System;
using UnityEngine;

namespace BattleShip.Manager
{
    [RequireComponent(typeof(ObjectEditor))]
    public class ObjectEditorManager : MonoBehaviour
    {
        public enum EditMode
        {
            On,
            Off
        }

        private EditMode _currentMode;
        private ObjectEditor _objectEditor;
        private Rigidbody _rigidbody;

        private void Awake()
        {
            _objectEditor = GetComponent<ObjectEditor>();
            _rigidbody = GetComponent<Rigidbody>();
        }

        private void Start()
        {
            SetEditMode(EditMode.Off);
        }

#if UNITY_EDITOR
        private void Update()
        {
            if (Input.GetKeyDown(KeyCode.X)) ToggleEditMode();
        }
#endif


        public void ToggleEditMode()
        {
            SetEditMode(_currentMode == EditMode.On ? EditMode.Off : EditMode.On);
        }

        public void SetEditMode(EditMode editMode)
        {
            _currentMode = editMode;

            var isOn = editMode == EditMode.On;
            _rigidbody.isKinematic = isOn;
            _objectEditor.enabled = isOn;
        }
    }
}