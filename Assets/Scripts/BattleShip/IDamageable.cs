﻿namespace BattleShip
{
    public interface IDamageable
    {
        void Damage(float damage);
    }
}