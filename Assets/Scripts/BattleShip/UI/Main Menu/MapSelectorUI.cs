﻿using BattleShip.Manager;
using BattleShip.Map.ScriptableObjects;
using BattleShip.System;
using UnityEngine;
using UnityEngine.UI;

namespace BattleShip.UI.Main_Menu
{
    public class MapSelectorUI : BaseUI
    {
        [SerializeField] private MapIcon mapIconTemplate;
        [SerializeField] private Transform mapContainer;
        [SerializeField] private Button openButton;
        [SerializeField] private Button closeButton;

        private void Awake()
        {
            Debug.Assert(closeButton != null, "closeButton can't be null!");
            Debug.Assert(mapIconTemplate != null, "mapIconTemplate can't be null!");
            Debug.Assert(mapContainer != null, "mapContainer can't be null!");
            
        }

        public void Init(MapConfig[] mapConfigs)
        {
            foreach (var config in mapConfigs)
            {
                var mapIcon = Instantiate(mapIconTemplate, mapContainer);
                mapIcon.Init(config.MapName, config.MapCover, config.SceneId);
            }

            mapIconTemplate.gameObject.SetActive(false);
            
            
            openButton.onClick.AddListener(() =>
            {
                Show();
                SoundManager.Play(SoundType.Button);
                FadeScreen.FadeIn(0.5f);
            });
            
            closeButton.onClick.AddListener(() =>
            {
                Hide();
                SoundManager.Play(SoundType.Button);
            });
        }
    }
}