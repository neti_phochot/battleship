﻿using UnityEngine;

namespace BattleShip.UI.Game
{
    public class HealthBarUI : BarUI
    {
        [SerializeField] private Gradient barColor;

        public override void Awake()
        {
            base.Awake();
            UpdateBarColor();
        }

        public override void SetFillAmount(float value)
        {
            base.SetFillAmount(value);
            UpdateBarColor();
        }

        private void UpdateBarColor()
        {
            barImage.color = barColor.Evaluate(barImage.fillAmount);
        }
    }
}