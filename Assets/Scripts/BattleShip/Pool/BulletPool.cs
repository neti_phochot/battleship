﻿using Singleton;
using UnityEngine;

namespace BattleShip.Pool
{
    public class BulletPool : MonoSingleton<BulletPool>, IPool<Bullet>
    {
        [SerializeField] private int initBullet;
        [SerializeField] private Bullet bulletPrefab;

        private Bullet[] _bulletsPool;
        private int _poolIndex;

        public override void Awake()
        {
            base.Awake();
            Debug.Assert(bulletPrefab != null, "bulletPrefab can't be null!");
            InitWarm(initBullet);
        }

        public void InitWarm(int amount)
        {
            _bulletsPool = new Bullet[amount];
            for (var i = 0; i < amount; i++)
            {
                var bullet = Instantiate(bulletPrefab);
                bullet.gameObject.SetActive(false);
                _bulletsPool[i] = bullet;
            }
        }

        public Bullet Request()
        {
            var bullet = _bulletsPool[_poolIndex++ % _bulletsPool.Length];
            bullet.gameObject.SetActive(true);
            return bullet;
        }

        public void Return(Bullet effect)
        {
            effect.gameObject.SetActive(false);
        }
    }
}