﻿using System;
using BattleShip.Pool;
using UnityEngine;

namespace BattleShip
{
    [RequireComponent(typeof(Rigidbody))]
    public class Bullet : Entity
    {
        private Rigidbody _rigidbody;
        public float Damage { get; set; }

        private void Awake()
        {
            _rigidbody = GetComponent<Rigidbody>();
        }

        private void OnCollisionEnter(Collision other)
        {
            OnHit?.Invoke(other);

            //De-active bullet and return to pool
            BulletPool.Instance.Return(this);

            if (!other.transform.TryGetComponent<IDamageable>(out var damageable)) return;
            damageable.Damage(Damage);
        }

        public event Action<Collision> OnHit;

        public void Init(Vector3 origin, Vector3 velocity, float damage)
        {
            transform.position = origin;
            _rigidbody.velocity = velocity;
            Damage = damage;
            OnHit = null;
        }
    }
}