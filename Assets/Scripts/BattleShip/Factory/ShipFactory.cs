﻿using System;
using BattleShip.Ship;
using Singleton;
using UnityEngine;

namespace BattleShip.Factory
{
    public class ShipFactory : MonoSingleton<ShipFactory>
    {
        public enum ShipType
        {
            Small,
            Medium,
            Large
        }

        [SerializeField] private ShipPrefabData smallShipPrefab;
        [SerializeField] private ShipPrefabData mediumShipPrefab;
        [SerializeField] private ShipPrefabData largeShipPrefab;

        public override void Awake()
        {
            base.Awake();

            Debug.Assert(smallShipPrefab.ship, "smallShipPrefab.ship can't be null!");
            Debug.Assert(mediumShipPrefab.ship, "mediumShipPrefab.ship can't be null!");
            Debug.Assert(largeShipPrefab.ship, "largeShipPrefab.ship can't be null!");

            Debug.Assert(smallShipPrefab.styles.Length == 2, "smallShipPrefab.style must assign at at 2 material");
            Debug.Assert(mediumShipPrefab.styles.Length == 2, "mediumShipPrefab.style must assign at at 2 material");
            Debug.Assert(largeShipPrefab.styles.Length == 2, "largeShipPrefab.style must assign at at 2 material");
        }

        public ShipBase Create(ShipType shipType, int styleIndex = 0)
        {
            ShipPrefabData shipPrefab;

            switch (shipType)
            {
                case ShipType.Small:
                    shipPrefab = smallShipPrefab;
                    break;
                case ShipType.Medium:
                    shipPrefab = mediumShipPrefab;
                    break;
                case ShipType.Large:
                    shipPrefab = largeShipPrefab;
                    break;
                default:
                    throw new ArgumentOutOfRangeException(nameof(shipType), shipType, null);
            }

            var ship = Instantiate(shipPrefab.ship);
            ship.GetComponent<ShipStyle>().SetStyle(shipPrefab.GetStyle(styleIndex));

            return ship;
        }

        public ShipBase[] Create(ShipType[] shipTypes, int styleIndex = 0)
        {
            var ships = new ShipBase[shipTypes.Length];
            for (var i = 0; i < shipTypes.Length; i++)
            {
                var shipType = shipTypes[i];
                var ship = Create(shipType, styleIndex);
                ships[i] = ship;
            }

            return ships;
        }

        [Serializable]
        public struct ShipPrefabData
        {
            public ShipBase ship;
            public Material[] styles;

            public Material GetStyle(int index)
            {
                return styles[index];
            }
        }
    }
}